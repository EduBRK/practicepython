# Character Input Exercise
# https://www.practicepython.org/exercise/2014/01/29/01-character-input.html
# Execute in Python 3!

# Imports
import sys
sys.path.append('../libs')

from Handler import empty_input_handler, exclusive_type_input_handler
from StringUtil import contains_any_digit

# Global Variables
# String values
name = None
age = None
repeat = None

# Integer Values
target_age = 100

# Functions
# Welcome message for program initialization.
def start_message():
	message = 'Iniciando exercício 1 - Character-Input '
	message += '(https://www.practicepython.org/exercise/2014/01/29/01-character-input.html)'
	print(message)

# Function to handle the process to ask for the user name.
def ask_name():
	global name
	valid = False

	while not valid:

		name = input('Escreva seu nome (Somente caracteres): ')

		if not name.strip():
			empty_input_handler('Nome')
		elif contains_any_digit(name):
			exclusive_type_input_handler('nome', 'caracteres')
		else:
			valid = True

# Function to handle the process to ask the age of the user.
def ask_age():
	global age
	valid = False

	while not valid:

		age = input('Digite a sua idade (Somente Números): ')

		if not age.strip():
			empty_input_handler('Idade')
		elif not age.isdigit():
			exclusive_type_input_handler('idade', 'números')
		else:
			valid = True

# Function to get a number of repetitions that the show_diffference function will show it's result.
def ask_repetitions():
	global repeat
	valid = False

	while not valid:

		repeat = input('Digite quantas vezes você deseja que a mensagem final seja escrita (Somente números): ') 

		if not repeat.strip():
			empty_input_handler('Repetições')
		elif not repeat.isdigit():
			exclusive_type_input_handler('repetições', 'números')
		else:
			valid = True

# Function that show the age difference of the user and repeats the phrase based on the repeat global variable.
def show_difference():
	global age
	global target_age
	global repeat

	difference = target_age - int(age)

	if difference == 0:
		message = '%s, você tem exatamente %s anos!!' % (name, target_age)
	elif difference > 0:
		message = '%s, faltam %s anos para que você faça %s anos!' % (name, difference, target_age)
	elif difference < 0:
		message = '%s, você tem %s anos mais do que %s anos!!' % (name, -difference, target_age)

	repeat = int(repeat)

	# Extra 1
	print(message * repeat)

	# Extra 2
	for i in range(repeat):
		print(message)
		
# Main Function
def main():
	start_message()
	ask_name()	
	ask_age()
	ask_repetitions()
	show_difference()

# Main guard
if __name__ == "__main__":
	main()