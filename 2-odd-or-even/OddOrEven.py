# Odd or Even Exercise
# https://www.practicepython.org/exercise/2014/01/29/02-odd-or-even.html
# Execute in Python 3!

# Imports
import sys
sys.path.append('../libs')

from Handler import empty_input_handler, exclusive_type_input_handler

# Global Variables
# String values
number = None
multiple = None

# Function to request a number from the user.
def ask_number():
	global number
	valid = False

	while not valid:

		number = input('Digite um número (Somente Números): ')

		if not number.strip():
			empty_input_handler('número')
		elif not number.isdigit():
			exclusive_type_input_handler('Número', 'números')
		else:
			valid = True

# Function to aswer if the inputed number is even or odd.
def answer_even_or_odd():
	global number
	module = int(number) % 2

	if module == 0:
		print('Este número (%s) é par!' % number)
	else:
		print('Este número (%s) é ímpar!' % number)

# Extra 1
# Function to answer if the input number is a multiple of a second number.
def answer_multiple(multiple):
	global number
	module = int(number) % multiple

	print(module)
	
	if module == 0:
		print('Este número (%s) é um multiplo de %s!' % (number, multiple))
	else:
		print('Este número (%s) não é um multiplo de %s!' % (number, multiple))

# Extra 2
# Function to ask the user for a number for divisible checking
def ask_multiple():
	global multiple
	valid = False

	while not valid:

		multiple = input('Digite um multiplo (Somente Números): ')

		if not multiple.strip():
			empty_input_handler('multiplo')
		elif not multiple.isdigit():
			exclusive_type_input_handler('Multiplo', 'números')
		else:
			valid = True

# Main Function
def main():
	global multiple

	ask_number()
	answer_even_or_odd()
	# Extra 1
	answer_multiple(4)

	# Extra 2
	ask_multiple()
	answer_multiple(int(multiple))

# Main guard
if __name__ == "__main__":
	main()