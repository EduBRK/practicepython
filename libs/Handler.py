# Handler Library
# Library with functions to handle errors and reusable behaviors

# Functions
# Function to handle empty inputs.
def empty_input_handler(value):
	print('Não é possível inserir um %s vazio. Por favor, tente novamente...' % value)
		
# Function to handle exclusive type inputs.
def exclusive_type_input_handler(value, valueType):
	print('%s deve conter somente %s, tente novamente...' % (value, valueType))