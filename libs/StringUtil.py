# String Util Lib
# Library for utilitary functions and lambdas about string manipulation

# Lambdas
# Lambda that checks if a given string contains any digit
contains_any_digit = lambda value : any(index.isdigit() for index in value)